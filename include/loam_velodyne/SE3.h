#include <Eigen/Core>
#include <Eigen/Geometry>

namespace loam {

class SE3Quat {
 private:
  using number_t = double;
  using cst = number_t;
  using Quaternion = Eigen::Quaternion<number_t>;
  using Vector3 = Eigen::Matrix<number_t, 3, 1>;
  using Vector7 = Eigen::Matrix<number_t, 7, 1>;
  using Vector6 = Eigen::Matrix<number_t, 6, 1>;
  using Matrix3 = Eigen::Matrix<number_t, 3, 3>;

  static Matrix3 skew(const Vector3& v) {
    Matrix3 result;
    result << 0, -v(2), v(1), v(2), 0, -v(0), -v(1), v(0), 0;
    return result;
  }

  static Eigen::Quaterniond Axisangle2Quaternion(
      const Eigen::Vector3d& axis_angle) {
    double theta = axis_angle.norm();

    double factor = std::sin(theta / 2) / theta;

    return (theta == 0) ? Eigen::Quaterniond(1, 0, 0, 0)
                        : Eigen::Quaterniond(
                              cos(theta / 2), axis_angle[0] * factor,
                              axis_angle[1] * factor, axis_angle[2] * factor);
  }

  static Eigen::Vector3d Quaternion2Axisangle(
      const Eigen::Quaterniond& quaternion) {
    Eigen::AngleAxisd aa(quaternion);
    Eigen::Vector3d axis_angle = aa.angle() * aa.axis();
    return axis_angle;
  }

 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

 public:
  SE3Quat() {
    _r.setIdentity();
    _t.setZero();
  }

  SE3Quat(const Matrix3& R, const Vector3& t) : _r(Quaternion(R)), _t(t) {
    normalizeRotation();
  }

  SE3Quat(const Quaternion& q, const Vector3& t) : _r(q), _t(t) {
    normalizeRotation();
  }

  inline const Vector3& translation() { return _t; }

  inline const Quaternion& rotation() { return _r; }

  inline SE3Quat operator*(const SE3Quat& tr2) {
    SE3Quat result(*this);
    result._t += _r * tr2._t;
    result._r *= tr2._r;
    result.normalizeRotation();
    return result;
  }

  inline SE3Quat& operator*=(const SE3Quat& tr2) {
    _t += _r * tr2._t;
    _r *= tr2._r;
    normalizeRotation();
    return *this;
  }

  inline Vector3 operator*(const Vector3& v) { return _t + _r * v; }

  inline SE3Quat inverse() {
    SE3Quat ret;
    ret._r = _r.conjugate();
    ret._t = ret._r * (_t * -cst(1.));
    return ret;
  }

  Vector6 log() {
    Vector6 res;
    Matrix3 _R = _r.toRotationMatrix();
    number_t d = cst(0.5) * (_R(0, 0) + _R(1, 1) + _R(2, 2) - 1);
    Vector3 omega;
    Vector3 upsilon;

    Vector3 dR = deltaR(_R);
    Matrix3 V_inv;

    if (d > cst(0.99999)) {
      omega = 0.5 * dR;
      Matrix3 Omega = skew(omega);
      V_inv = Matrix3::Identity() - cst(0.5) * Omega +
              (cst(1.) / cst(12.)) * (Omega * Omega);
    } else {
      number_t theta = std::acos(d);
      omega = theta / (2 * std::sqrt(1 - d * d)) * dR;
      Matrix3 Omega = skew(omega);
      V_inv = (Matrix3::Identity() - cst(0.5) * Omega +
               (1 - theta / (2 * std::tan(theta / 2))) / (theta * theta) *
                   (Omega * Omega));
    }

    upsilon = V_inv * _t;
    for (int i = 0; i < 3; i++) {
      res[i] = omega[i];
    }
    for (int i = 0; i < 3; i++) {
      res[i + 3] = upsilon[i];
    }

    return res;
  }

  inline Vector3 map(const Vector3& xyz) { return _r * xyz + _t; }

  // 先旋转后平移
  static SE3Quat exp(const Vector6& update) {
    Vector3 omega;
    for (int i = 0; i < 3; i++) omega[i] = update[i];
    Vector3 upsilon;
    for (int i = 0; i < 3; i++) upsilon[i] = update[i + 3];

    number_t theta = omega.norm();
    Matrix3 Omega = skew(omega);

    Matrix3 R;
    Matrix3 V;
    if (theta < cst(0.00001)) {
      Matrix3 Omega2 = Omega * Omega;

      R = (Matrix3::Identity() + Omega + cst(0.5) * Omega2);

      V = (Matrix3::Identity() + cst(0.5) * Omega + cst(1.) / cst(6.) * Omega2);
    } else {
      Matrix3 Omega2 = Omega * Omega;

      R = (Matrix3::Identity() + std::sin(theta) / theta * Omega +
           (1 - std::cos(theta)) / (theta * theta) * Omega2);

      V = (Matrix3::Identity() +
           (1 - std::cos(theta)) / (theta * theta) * Omega +
           (theta - std::sin(theta)) / (std::pow(theta, 3)) * Omega2);
    }
    return SE3Quat(Quaternion(R), V * upsilon);
  }

 private:
  void normalizeRotation() {
    if (_r.w() < 0) {
      _r.coeffs() *= -1;
    }
    _r.normalize();
  }

  Vector3 deltaR(const Matrix3& R) {
    Vector3 v;
    v(0) = R(2, 1) - R(1, 2);
    v(1) = R(0, 2) - R(2, 0);
    v(2) = R(1, 0) - R(0, 1);
    return v;
  }

 private:
  Quaternion _r;
  Vector3 _t;
};
}
