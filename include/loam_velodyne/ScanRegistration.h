// Copyright 2013, Ji Zhang, Carnegie Mellon University
// Further contributions copyright (c) 2016, Southwest Research Institute
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from this
//    software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// This is an implementation of the algorithm described in the following paper:
//   J. Zhang and S. Singh. LOAM: Lidar Odometry and Mapping in Real-time.
//     Robotics: Science and Systems Conference (RSS). Berkeley, CA, July 2014.

#ifndef LOAM_SCANREGISTRATION_H
#define LOAM_SCANREGISTRATION_H


#include "common.h"
#include "Angle.h"
#include "Vector3.h"
#include "CircularBuffer.h"

#include <stdint.h>
#include <vector>
#include <ros/node_handle.h>
#include <sensor_msgs/Imu.h>
#include <pcl/point_cloud.h>


namespace loam {

/** \brief A pair describing the start end end index of a range. */
typedef std::pair<size_t, size_t> IndexRange;



/** Point label options. */
enum PointLabel {
  CORNER_SHARP = 2,       ///< sharp corner point
  CORNER_LESS_SHARP = 1,  ///< less sharp corner point
  SURFACE_LESS_FLAT = 0,  ///< less flat surface point
  SURFACE_FLAT = -1       ///< flat surface point
};



/** Scan Registration configuration parameters. */
class RegistrationParams {
public:
  RegistrationParams(const float& scanPeriod_ = 0.1,
                     const int& imuHistorySize_ = 200,
                     const int& nFeatureRegions_ = 6,
                     const int& curvatureRegion_ = 5,
                     const int& maxCornerSharp_ = 2,
                     const int& maxSurfaceFlat_ = 4,
                     const float& lessFlatFilterSize_ = 0.2,
                     const float& surfaceCurvatureThreshold_ = 0.1);


  /** \brief Parse node parameter.
   *
   * @param nh the ROS node handle
   * @return true, if all specified parameters are valid, false if at least one specified parameter is invalid
   */
  bool parseParams(const ros::NodeHandle& nh);

  /** The time per scan. */
  // 扫描周期，默认0.1
  float scanPeriod;

  /** The size of the IMU history state buffer. */
  // IMU历史数据个数，默认200
  int imuHistorySize;

  /** The number of (equally sized) regions used to distribute the feature extraction within a scan. */
  // 每条扫描线均分的区域数，默认6
  int nFeatureRegions;

  /** The number of surrounding points (+/- region around a point) used to calculate a point curvature. */
  // 计算曲率的区域，默认5
  int curvatureRegion;

  /** The maximum number of sharp corner points per feature region. */
  // 每个区域的最大边角点个数，默认2
  int maxCornerSharp;

  /** The maximum number of less sharp corner points per feature region. */
  // 每个区域的最大似边角点个数，默认20
  int maxCornerLessSharp;

  /** The maximum number of flat surface points per feature region. */
  // 每个区域的最大平面点个数，默认4
  int maxSurfaceFlat;

  /** The voxel size used for down sizing the remaining less flat surface points. */
  // 似平面点过滤器尺寸，默认0.2
  float lessFlatFilterSize;

  /** The curvature threshold below / above a point is considered a flat / corner point. */
  // 曲率阈值，大于阈值的是边角点，小于阈值的是平面点，默认0.1
  float surfaceCurvatureThreshold;
};



/** IMU state data. */
typedef struct IMUState {
  /** The time of the measurement leading to this state (in seconds). */
  ros::Time stamp;

  /** The current roll angle. */
  Angle roll;

  /** The current pitch angle. */
  Angle pitch;

  /** The current yaw angle. */
  Angle yaw;

  /** The accumulated global IMU position in 3D space. */
  Vector3 position;

  /** The accumulated global IMU velocity in 3D space. */
  Vector3 velocity;

  /** The current (local) IMU acceleration in 3D space. */
  Vector3 acceleration;

  /** \brief Interpolate between two IMU states.
   *
   * @param start the first IMUState
   * @param end the second IMUState
   * @param ratio the interpolation ratio
   * @param result the target IMUState for storing the interpolation result
   */
  static void interpolate(const IMUState& start,
                          const IMUState& end,
                          const float& ratio,
                          IMUState& result)
  {
    float invRatio = 1 - ratio;

    result.roll = start.roll.rad() * invRatio + end.roll.rad() * ratio;
    result.pitch = start.pitch.rad() * invRatio + end.pitch.rad() * ratio;
    if (start.yaw.rad() - end.yaw.rad() > M_PI) {
      result.yaw = start.yaw.rad() * invRatio + (end.yaw.rad() + 2 * M_PI) * ratio;
    } else if (start.yaw.rad() - end.yaw.rad() < -M_PI) {
      result.yaw = start.yaw.rad() * invRatio + (end.yaw.rad() - 2 * M_PI) * ratio;
    } else {
      result.yaw = start.yaw.rad() * invRatio + end.yaw.rad() * ratio;
    }

    result.velocity = start.velocity * invRatio + end.velocity * ratio;
    result.position = start.position * invRatio + end.position * ratio;
  };
} IMUState;



/** \brief Base class for LOAM scan registration implementations.
 *
 * As there exist various sensor devices, producing differently formatted point clouds,
 * specific implementations are needed for each group of sensor devices to achieve an accurate registration.
 * This class provides common configurations, buffering and processing logic.
 */
// LOAM的扫描线配准器
class ScanRegistration {
public:
  explicit ScanRegistration(const RegistrationParams& config = RegistrationParams());

  /** \brief Setup component.
   *
   * @param node the ROS node handle
   * @param privateNode the private ROS node handle
   */
  // 设置节点的参数，设置订阅和发布主题名
  virtual bool setup(ros::NodeHandle& node,
                     ros::NodeHandle& privateNode);

  /** \brief Handler method for IMU messages.
   *
   * @param imuIn the new IMU message
   */
  // 处理IMU数据
  virtual void handleIMUMessage(const sensor_msgs::Imu::ConstPtr& imuIn);


protected:
  /** \brief Prepare for next scan / sweep.
   *
   * @param scanTime the current scan time
   * @param newSweep indicator if a new sweep has started
   */
  // 准备下一个 scan / sweep
  void reset(const ros::Time& scanTime,
             const bool& newSweep = true);

  /** \breif Check is IMU data is available. */
  // 查看是否有IMU数据
  inline bool hasIMUData() { return imu_history_.size() > 0; };

  /** \brief Set up the current IMU transformation for the specified relative time.
   *
   * @param relTime the time relative to the scan time
   */
  void setIMUTransformFor(const float& relTime);

  /** \brief Project the given point to the start of the sweep, using the current IMU state and position shift.
   *
   * @param point the point to project
   */
  void transformToStartIMU(pcl::PointXYZI& point);

  /** \brief Extract features from current laser cloud.
   *
   * @param beginIdx the index of the first scan to extract features from
   */
  // 提取特征点
  void extractFeatures(const uint16_t& beginIdx = 0);

  /** \brief Set up region buffers for the specified point range.
   *
   * @param startIdx the region start index
   * @param endIdx the region end index
   */
  void setRegionBuffersFor(const size_t& startIdx,
                           const size_t& endIdx);

  /** \brief Set up scan buffers for the specified point range.
   *
   * @param startIdx the scan start index
   * @param endIdx the scan start index
   */
  void setScanBuffersFor(const size_t& startIdx,
                         const size_t& endIdx);

  /** \brief Mark a point and its neighbors as picked.
   *
   * This method will mark neighboring points within the curvature region as picked,
   * as long as they remain within close distance to each other.
   *
   * @param cloudIdx the index of the picked point in the full resolution cloud
   * @param scanIdx the index of the picked point relative to the current scan
   */
  void markAsPicked(const size_t& cloudIdx,
                    const size_t& scanIdx);

  /** \brief Publish the current result via the respective topics. */
  void publishResult();


private:
  /** \brief Try to interpolate the IMU state for the given time.
   *
   * @param relTime the time relative to the scan time
   * @param outputState the output state instance
   */
  void interpolateIMUStateFor(const float& relTime,
                              IMUState& outputState);


protected:
  RegistrationParams config_;   ///< registration parameter

  ros::Time sweep_starttime_;              ///< time stamp of beginning of current sweep
  ros::Time scantime_;                     ///< time stamp of most recent scan
  IMUState imu_start_;                     ///< the interpolated IMU state corresponding to the start time of the currently processed laser scan
  IMUState imu_cur_;                       ///< the interpolated IMU state corresponding to the time of the currently processed laser scan point
  Vector3 imu_pos_shift_;                  ///< position shift between accumulated IMU position and interpolated IMU position
  size_t imu_idx_;                         ///< the current index in the IMU history
  CircularBuffer<IMUState> imu_history_;   ///< history of IMU states for cloud registration

  // 有序的点云，从下到上，固定水平旋转方向（顺时针/逆时针皆可）扫描
  pcl::PointCloud<pcl::PointXYZI> laser_pointcloud_;    ///< full resolution input cloud
  // 每个扫描线在点云中的索引范围，向量大小和扫描线个数相同
  std::vector<IndexRange> scan_indices_;          ///< start and end indices of the individual scans withing the full resolution cloud

  pcl::PointCloud<pcl::PointXYZI> cornerpoint_sharp_;       ///< sharp corner points cloud
  pcl::PointCloud<pcl::PointXYZI> cornerpoint_less_sharp_;  ///< less sharp corner points cloud
  pcl::PointCloud<pcl::PointXYZI> surfacepoint_flat_;       ///< flat surface points cloud
  pcl::PointCloud<pcl::PointXYZI> surfacepoint_less_flat_;  ///< less flat surface points cloud
  pcl::PointCloud<pcl::PointXYZ> imu_trans_;                ///< IMU transformation information

  std::vector<float> region_curvatures_;      ///< point curvature buffer
  std::vector<PointLabel> region_labels_;     ///< point label buffer
  std::vector<size_t> region_sort_indices_;   ///< sorted region indices based on point curvature
  std::vector<int> scan_neighbor_picked_;     ///< flag if neighboring point was already picked

  ros::Subscriber imu_subscriber_;    ///< IMU message subscriber

  ros::Publisher laser_pointcloud_publisher_;               ///< full resolution cloud message publisher
  ros::Publisher cornerpoint_sharp_publisher_;        ///< sharp corner cloud message publisher
  ros::Publisher cornerpoint_less_sharp_publisher_;   ///< less sharp corner cloud message publisher
  ros::Publisher surfacepoint_flat_publisher_;        ///< flat surface cloud message publisher
  ros::Publisher surfacepoint_less_flat_publisher_;   ///< less flat surface cloud message publisher
  ros::Publisher imu_trans_publisher_;                ///< IMU transformation message publisher
};

} // end namespace loam


#endif //LOAM_SCANREGISTRATION_H

