// Copyright 2013, Ji Zhang, Carnegie Mellon University
// Further contributions copyright (c) 2016, Southwest Research Institute
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from this
//    software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// This is an implementation of the algorithm described in the following paper:
//   J. Zhang and S. Singh. LOAM: Lidar Odometry and Mapping in Real-time.
//     Robotics: Science and Systems Conference (RSS). Berkeley, CA, July 2014.

#ifndef LOAM_LASERODOMETRY_H
#define LOAM_LASERODOMETRY_H

#include "loam_velodyne/Twist.h"
#include "loam_velodyne/nanoflann_pcl.h"

#include <nav_msgs/Odometry.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <ros/node_handle.h>
#include <sensor_msgs/PointCloud2.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_datatypes.h>
#include "SE3.h"

namespace loam {

class LaserOdometryCostFunction;

/** \brief Implementation of the LOAM laser odometry component.
 *
 */
class LaserOdometry {
 public:
  explicit LaserOdometry(const float& scanPeriod = 0.1,
                         const uint16_t& ioRatio = 2,
                         const size_t& maxIterations = 15);

  /** \brief Setup component.
   *
   * @param node the ROS node handle
   * @param privateNode the private ROS node handle
   */
  virtual bool setup(ros::NodeHandle& node, ros::NodeHandle& privateNode);

  /** \brief Handler method for a new sharp corner cloud.
   *
   * @param cornerPointsSharpMsg the new sharp corner cloud message
   */
  void laserCloudSharpHandler(
      const sensor_msgs::PointCloud2ConstPtr& cornerPointsSharpMsg);

  /** \brief Handler method for a new less sharp corner cloud.
   *
   * @param cornerPointsLessSharpMsg the new less sharp corner cloud message
   */
  void laserCloudLessSharpHandler(
      const sensor_msgs::PointCloud2ConstPtr& cornerPointsLessSharpMsg);

  /** \brief Handler method for a new flat surface cloud.
   *
   * @param surfPointsFlatMsg the new flat surface cloud message
   */
  void laserCloudFlatHandler(
      const sensor_msgs::PointCloud2ConstPtr& surfPointsFlatMsg);

  /** \brief Handler method for a new less flat surface cloud.
   *
   * @param surfPointsLessFlatMsg the new less flat surface cloud message
   */
  void laserCloudLessFlatHandler(
      const sensor_msgs::PointCloud2ConstPtr& surfPointsLessFlatMsg);

  /** \brief Handler method for a new full resolution cloud.
   *
   * @param laserCloudFullResMsg the new full resolution cloud message
   */
  void laserCloudFullResHandler(
      const sensor_msgs::PointCloud2ConstPtr& laserCloudFullResMsg);

  /** \brief Process incoming messages in a loop until shutdown (used in active
   * mode). */
  void spin();

  /** \brief Try to process buffered data. */
  void process();

 protected:
  /** \brief Reset flags, etc. */
  void reset();

  /** \brief Check if all required information for a new processing step is
   * available. */
  bool hasNewData();

  /** \brief Publish the current result via the respective topics. */
  void publishResult();

 private:
  /**
   * \brief updateIndices
   *
   * Update point_search_corner_idxN_ and point_seach_surface_idxN_
   *
   */
  void updateIndices(SE3Quat);

  /**
   * \brief updateTransformedPointCloud
   *
   * Update corner_points_sharp_transformed_ and
   * surface_points_flat_transformed_
   *
   */
  void updateTransformedPointCloud(SE3Quat);

  /**
   * \brief computeSquaredError
   *
   * compute Error (Rt * R / 2)
   *
   */
  double computeSquaredError(double&);

  /**
   * \brief computeSquaredError
   *
   * compute Jtj (Jt * J), Grad (Jt * R), Error (Rt * R / 2)
   *
   */
  void computeJtjGradError(Eigen::Matrix<double, 6, 6>& matJtJ,
                           Eigen::Matrix<double, 6, 1>& matJtR,
                           double& error_cost, int& pp_pl_total_count);

 private:
  // clang-format off
  float scan_period_;      ///< time per scan
  uint16_t io_ratio_;      ///< ratio of input to output frames
  bool is_system_inited_;  ///< initialization flag
  long frame_count_;       ///< number of processed frames

  size_t max_iterations_;  ///< maximum number of iterations
  float deltaT_abort_;     ///< optimization abort threshold for deltaT
  float deltaR_abort_;     ///< optimization abort threshold for deltaR

  pcl::PointCloud<pcl::PointXYZI>::Ptr corner_points_sharp_;       ///< sharp corner points cloud
  pcl::PointCloud<pcl::PointXYZI>::Ptr corner_points_less_sharp_;  ///< less sharp corner points cloud
  pcl::PointCloud<pcl::PointXYZI>::Ptr surface_points_flat_;       ///< flat surface points cloud
  pcl::PointCloud<pcl::PointXYZI>::Ptr surface_points_less_flat_;  ///< less flat surface points cloud
  pcl::PointCloud<pcl::PointXYZI>::Ptr laser_cloud_;               ///< full resolution cloud

  pcl::PointCloud<pcl::PointXYZI>::Ptr last_corner_cloud_;         ///< last corner points cloud
  pcl::PointCloud<pcl::PointXYZI>::Ptr last_surface_cloud_;        ///< last surface points cloud

  nanoflann::KdTreeFLANN<pcl::PointXYZI> last_corner_KDTree_;   ///< last corner cloud KD-tree
  nanoflann::KdTreeFLANN<pcl::PointXYZI> last_surface_KDTree_;  ///< last surface cloud KD-tree

  ros::Time time_corner_points_sharp_;        ///< time of current sharp corner cloud
  ros::Time time_corner_points_less_sharp_;   ///< time of current less sharp corner cloud
  ros::Time time_surface_points_flat_;        ///< time of current flat surface cloud
  ros::Time time_surface_points_less_flat_;   ///< time of current less flat surface cloud
  ros::Time time_laser_cloud_fullres_;        ///< time of current full resolution cloud

  bool has_new_corner_points_sharp_;        ///< flag if a new sharp corner cloud has been received
  bool has_new_corner_points_less_sharp_;   ///< flag if a new less sharp corner cloud has been received
  bool has_new_surface_points_flat_;        ///< flag if a new flat surface cloud has been received
  bool has_new_surface_points_less_flat_;   ///< flag if a new less flat surface cloud has been received
  bool has_new_laser_cloud_fullres_;        ///< flag if a new full resolution cloud has been received

  std::vector<int> point_search_corner_idx1_;  ///< first corner point search index buffer
  std::vector<int> point_search_corner_idx2_;  ///< second corner point search index buffer
  std::vector<pcl::PointXYZI> corner_points_sharp_transformed_;

  std::vector<int> point_seach_surface_idx1_;  ///< first surface point search index buffer
  std::vector<int> point_seach_surface_idx2_;  ///< second surface point search index buffer
  std::vector<int> point_seach_surface_idx3_;  ///< third surface point search index buffer
  std::vector<pcl::PointXYZI> surface_points_flat_transformed_;

  ros::Publisher laser_cloud_corner_last_publisher_;  ///< last corner cloud message publisher
  ros::Publisher laser_cloud_surface_last_publisher_; ///< last surface cloud message publisher
  ros::Publisher laser_cloud_fullres_publisher_;      ///< full resolution cloud message publisher
  ros::Publisher laser_odom_publisher_;               ///< laser odometry publisher
  tf::TransformBroadcaster tf_broadcaster_;           ///< laser odometry transform broadcaster

  ros::Subscriber corner_points_sharp_subscriber_;       ///< sharp corner cloud message subscriber
  ros::Subscriber corner_points_less_sharp_subscriber_;  ///< less sharp corner cloud message subscriber
  ros::Subscriber surface_points_flat_subscriber_;       ///< flat surface cloud message subscriber
  ros::Subscriber surface_points_less_flat_subscriber_;  ///< less flat surface cloud message subscriber
  ros::Subscriber laser_cloud_fullres_subscriber_;       ///< full resolution cloud message subscriber

  loam::SE3Quat odom_acc_;
  // clang-format on

 public:
  friend class LaserOdometryCostFunction;
};

}  // end namespace loam

#endif  // LOAM_LASERODOMETRY_H
