// Copyright 2013, Ji Zhang, Carnegie Mellon University
// Further contributions copyright (c) 2016, Southwest Research Institute
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from this
//    software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// This is an implementation of the algorithm described in the following paper:
//   J. Zhang and S. Singh. LOAM: Lidar Odometry and Mapping in Real-time.
//     Robotics: Science and Systems Conference (RSS). Berkeley, CA, July 2014.

#include "loam_velodyne/LaserOdometry.h"
#include "loam_velodyne/common.h"
#include "loam_velodyne/math_utils.h"

#include <pcl/filters/filter.h>
#include <Eigen/Eigenvalues>
#include <Eigen/Geometry>
#include <Eigen/QR>

namespace loam {

LaserOdometry::LaserOdometry(const float& scanPeriod, const uint16_t& ioRatio,
                             const size_t& maxIterations)
    : scan_period_(scanPeriod),
      io_ratio_(ioRatio),
      is_system_inited_(false),
      frame_count_(0),
      max_iterations_(maxIterations),
      deltaT_abort_(0.1),
      deltaR_abort_(0.1),
      corner_points_sharp_(new pcl::PointCloud<pcl::PointXYZI>()),
      corner_points_less_sharp_(new pcl::PointCloud<pcl::PointXYZI>()),
      surface_points_flat_(new pcl::PointCloud<pcl::PointXYZI>()),
      surface_points_less_flat_(new pcl::PointCloud<pcl::PointXYZI>()),
      laser_cloud_(new pcl::PointCloud<pcl::PointXYZI>()),
      last_corner_cloud_(new pcl::PointCloud<pcl::PointXYZI>()),
      last_surface_cloud_(new pcl::PointCloud<pcl::PointXYZI>()) {}

bool LaserOdometry::setup(ros::NodeHandle& node, ros::NodeHandle& privateNode) {
  // fetch laser odometry params
  float fParam;
  int iParam;

  if (privateNode.getParam("scanPeriod", fParam)) {
    if (fParam <= 0) {
      ROS_ERROR("Invalid scanPeriod parameter: %f (expected > 0)", fParam);
      return false;
    } else {
      scan_period_ = fParam;
      ROS_INFO("Set scanPeriod: %g", fParam);
    }
  }

  if (privateNode.getParam("ioRatio", iParam)) {
    if (iParam < 1) {
      ROS_ERROR("Invalid ioRatio parameter: %d (expected > 0)", iParam);
      return false;
    } else {
      io_ratio_ = iParam;
      ROS_INFO("Set ioRatio: %d", iParam);
    }
  }

  if (privateNode.getParam("maxIterations", iParam)) {
    if (iParam < 1) {
      ROS_ERROR("Invalid maxIterations parameter: %d (expected > 0)", iParam);
      return false;
    } else {
      max_iterations_ = iParam;
      ROS_INFO("Set maxIterations: %d", iParam);
    }
  }

  if (privateNode.getParam("deltaTAbort", fParam)) {
    if (fParam <= 0) {
      ROS_ERROR("Invalid deltaTAbort parameter: %f (expected > 0)", fParam);
      return false;
    } else {
      deltaT_abort_ = fParam;
      ROS_INFO("Set deltaTAbort: %g", fParam);
    }
  }

  if (privateNode.getParam("deltaRAbort", fParam)) {
    if (fParam <= 0) {
      ROS_ERROR("Invalid deltaRAbort parameter: %f (expected > 0)", fParam);
      return false;
    } else {
      deltaR_abort_ = fParam;
      ROS_INFO("Set deltaRAbort: %g", fParam);
    }
  }

  // advertise laser odometry topics
  laser_cloud_corner_last_publisher_ =
      node.advertise<sensor_msgs::PointCloud2>("/laser_cloud_corner_last", 2);
  laser_cloud_surface_last_publisher_ =
      node.advertise<sensor_msgs::PointCloud2>("/laser_cloud_surf_last", 2);
  laser_cloud_fullres_publisher_ =
      node.advertise<sensor_msgs::PointCloud2>("/velodyne_cloud_3", 2);
  laser_odom_publisher_ =
      node.advertise<nav_msgs::Odometry>("/laser_odom_to_init", 5);

  // subscribe to scan registration topics
  corner_points_sharp_subscriber_ = node.subscribe<sensor_msgs::PointCloud2>(
      "/laser_cloud_sharp", 2, &LaserOdometry::laserCloudSharpHandler, this);

  corner_points_less_sharp_subscriber_ =
      node.subscribe<sensor_msgs::PointCloud2>(
          "/laser_cloud_less_sharp", 2,
          &LaserOdometry::laserCloudLessSharpHandler, this);

  surface_points_flat_subscriber_ = node.subscribe<sensor_msgs::PointCloud2>(
      "/laser_cloud_flat", 2, &LaserOdometry::laserCloudFlatHandler, this);

  surface_points_less_flat_subscriber_ =
      node.subscribe<sensor_msgs::PointCloud2>(
          "/laser_cloud_less_flat", 2,
          &LaserOdometry::laserCloudLessFlatHandler, this);

  laser_cloud_fullres_subscriber_ = node.subscribe<sensor_msgs::PointCloud2>(
      "/velodyne_cloud_2", 2, &LaserOdometry::laserCloudFullResHandler, this);

  return true;
}

void LaserOdometry::laserCloudSharpHandler(
    const sensor_msgs::PointCloud2ConstPtr& cornerPointsSharpMsg) {
  time_corner_points_sharp_ = cornerPointsSharpMsg->header.stamp;

  corner_points_sharp_->clear();
  pcl::fromROSMsg(*cornerPointsSharpMsg, *corner_points_sharp_);
  std::vector<int> indices;
  pcl::removeNaNFromPointCloud(*corner_points_sharp_, *corner_points_sharp_,
                               indices);
  has_new_corner_points_sharp_ = true;
}

void LaserOdometry::laserCloudLessSharpHandler(
    const sensor_msgs::PointCloud2ConstPtr& cornerPointsLessSharpMsg) {
  time_corner_points_less_sharp_ = cornerPointsLessSharpMsg->header.stamp;

  corner_points_less_sharp_->clear();
  pcl::fromROSMsg(*cornerPointsLessSharpMsg, *corner_points_less_sharp_);
  std::vector<int> indices;
  pcl::removeNaNFromPointCloud(*corner_points_less_sharp_,
                               *corner_points_less_sharp_, indices);
  has_new_corner_points_less_sharp_ = true;
}

void LaserOdometry::laserCloudFlatHandler(
    const sensor_msgs::PointCloud2ConstPtr& surfPointsFlatMsg) {
  time_surface_points_flat_ = surfPointsFlatMsg->header.stamp;

  surface_points_flat_->clear();
  pcl::fromROSMsg(*surfPointsFlatMsg, *surface_points_flat_);
  std::vector<int> indices;
  pcl::removeNaNFromPointCloud(*surface_points_flat_, *surface_points_flat_,
                               indices);
  has_new_surface_points_flat_ = true;
}

void LaserOdometry::laserCloudLessFlatHandler(
    const sensor_msgs::PointCloud2ConstPtr& surfPointsLessFlatMsg) {
  time_surface_points_less_flat_ = surfPointsLessFlatMsg->header.stamp;

  surface_points_less_flat_->clear();
  pcl::fromROSMsg(*surfPointsLessFlatMsg, *surface_points_less_flat_);
  std::vector<int> indices;
  pcl::removeNaNFromPointCloud(*surface_points_less_flat_,
                               *surface_points_less_flat_, indices);
  has_new_surface_points_less_flat_ = true;
}

void LaserOdometry::laserCloudFullResHandler(
    const sensor_msgs::PointCloud2ConstPtr& laserCloudFullResMsg) {
  time_laser_cloud_fullres_ = laserCloudFullResMsg->header.stamp;

  laser_cloud_->clear();
  pcl::fromROSMsg(*laserCloudFullResMsg, *laser_cloud_);
  std::vector<int> indices;
  pcl::removeNaNFromPointCloud(*laser_cloud_, *laser_cloud_, indices);
  has_new_laser_cloud_fullres_ = true;
}

void LaserOdometry::spin() {
  ros::Rate rate(100);
  bool status = ros::ok();

  // loop until shutdown
  while (status) {
    ros::spinOnce();

    // try processing new data
    process();

    status = ros::ok();
    rate.sleep();
  }
}

void LaserOdometry::reset() {
  has_new_corner_points_sharp_ = false;
  has_new_corner_points_less_sharp_ = false;
  has_new_surface_points_flat_ = false;
  has_new_surface_points_less_flat_ = false;
  has_new_laser_cloud_fullres_ = false;
}

bool LaserOdometry::hasNewData() {
  return has_new_corner_points_sharp_ && has_new_corner_points_less_sharp_ &&
         has_new_surface_points_flat_ && has_new_surface_points_less_flat_ &&
         has_new_laser_cloud_fullres_ &&
         fabs((time_corner_points_sharp_ - time_surface_points_less_flat_)
                  .toSec()) < 0.005 &&
         fabs((time_corner_points_less_sharp_ - time_surface_points_less_flat_)
                  .toSec()) < 0.005 &&
         fabs((time_surface_points_flat_ - time_surface_points_less_flat_)
                  .toSec()) < 0.005 &&
         fabs((time_laser_cloud_fullres_ - time_surface_points_less_flat_)
                  .toSec()) < 0.005;
}

void LaserOdometry::process() {
  // TODO remove ALL nan values from ALL point cloud
  if (!hasNewData()) {
    // waiting for new data to arrive...
    return;
  }

  // reset flags, etc.
  reset();

  if (!is_system_inited_) {
    corner_points_less_sharp_.swap(last_corner_cloud_);
    surface_points_less_flat_.swap(last_surface_cloud_);

    last_corner_KDTree_.setInputCloud(last_corner_cloud_);
    last_surface_KDTree_.setInputCloud(last_surface_cloud_);

    is_system_inited_ = true;
    return;
  }

  frame_count_++;

  size_t lastCornerCloudSize = last_corner_cloud_->points.size();
  size_t lastSurfaceCloudSize = last_surface_cloud_->points.size();
  loam::SE3Quat odom_cur_;

  if (lastCornerCloudSize > 10 && lastSurfaceCloudSize > 100) {
    size_t cornerPointsSharpNum = corner_points_sharp_->points.size();
    size_t surfPointsFlatNum = surface_points_flat_->points.size();

    point_search_corner_idx1_.resize(cornerPointsSharpNum);
    point_search_corner_idx2_.resize(cornerPointsSharpNum);
    point_seach_surface_idx1_.resize(surfPointsFlatNum);
    point_seach_surface_idx2_.resize(surfPointsFlatNum);
    point_seach_surface_idx3_.resize(surfPointsFlatNum);
    corner_points_sharp_transformed_.resize(cornerPointsSharpNum);
    surface_points_flat_transformed_.resize(surfPointsFlatNum);

    // Matrix used in GN method
    Eigen::Matrix<double, 3, 6> matJ;
    Eigen::Matrix<double, 3, 1> matR;
    // matJtJ and matJtR MUST be set to zeros in every interation
    Eigen::Matrix<double, 6, 6> matJtJ;
    Eigen::Matrix<double, 6, 1> matJtR;

    /*
    double tao = 1e-3;
    double eps1 = 1e-12;
    double eps2 = 1e-12;
    int k = 0;
    long v = 2;
    VectorXd f = objF(input, output, x);
    MatrixXd J = Jacobin(input, output, x);  // jacobin
    MatrixXd A = J.transpose() * J;          // hessian
    <------------------------
    VectorXd g = J.transpose() * f;          // gradient
    <------------------------
    double u = tao * maxMatrixDiagonale(A);
    printf("\nLevenberg-Marquart Algorithm:\n");
    while (k++ < MAX_ITER) {
      if (g.norm() <= eps1) {
        cout << "stop g(x) = 0 for a local minimizer optimizer." << endl;
        break;
      }
      printf("%4d     %13.8f%13.8f%13.8f%13.8f%14.8f\n", k, x[0], x[1], x[2],
            x[3], Func(f));
      VectorXd h =
          -(A + u * MatrixXd::Identity(x.rows(), x.rows())).inverse() * g;
      //    std::cout << eps2 * (x.norm() + eps2) << ", " << h.transpose() <<
      //    std::endl;
      if (h.norm() <= eps2 * (x.norm() + eps2)) {
        cout << "stop because change in x is small" << endl;
        break;
      }
      VectorXd x_new = x + h;
      VectorXd F_new = objF(input, output, x_new);
      double deltaF = Func(f) - Func(F_new); <------------------------
      double deltaL = linerDeltaL(h, g, u);
      double rho = deltaF / deltaL;
      if (rho > 0) {
        // update x
        x = x_new;
        // update A g
        f = objF(input, output, x);
        J = Jacobin(input, output, x);
        A = J.transpose() * J; <------------------------
        g = J.transpose() * f; <------------------------
        // update u v
        u *= std::max(1.0 / 3.0, 1 - pow(2 * rho - 1, 3));
        v = 2;
      } else {
        u *= v;
        v *= 2;
      }
    }
     */

    int pp_pl_total_count;
    double error_cost;
    double tao = 1e-3;
    double eps1 = 1e-12;
    double eps2 = 1e-12;
    int k = 0;
    long v = 2;

    updateTransformedPointCloud(odom_cur_);
    updateIndices(odom_cur_);

    computeJtjGradError(matJtJ, matJtR, error_cost, pp_pl_total_count);

    double u = tao * matJtJ.diagonal().maxCoeff();

    // Use Guass-Newton Algorithm
    for (size_t iterCount = 0; iterCount < max_iterations_; iterCount++) {
      if (matJtR.norm() <= eps1) {
        std::cout << "stop g(x) = 0 for a local minimizer optimizer."
                  << std::endl;
        break;
      }

      // Solve equation "matJtJ X = - matJtR" by QR decomposition
      Eigen::Matrix<double, 6, 1> h =
          (pp_pl_total_count < 10)
              ? Eigen::Matrix<double, 6, 1>::Zero()
              : Eigen::Matrix<double, 6, 1>(
                    -(matJtJ + u * Eigen::MatrixXd::Identity(6, 6))
                         .colPivHouseholderQr()
                         .solve(matJtR));

      std::cout << pp_pl_total_count << "  " << error_cost << "  "
                << h.transpose() << std::endl;

      //       if (h.norm() <= eps2 * (x.norm() + eps2)) {
      //         std::cout << "stop because change in x is small" << std::endl;
      //         break;
      //       }

      // If delta_angle < 1e-3 and delta_translation < 5e-3, exit
      if (h.head<3>().norm() < 1e-3 && h.tail<3>().norm() < 5e-3) {
        std::cout << "stop because change in SE(3) is small" << std::endl;
        break;
      }

      loam::SE3Quat odom_new = loam::SE3Quat::exp(h) * odom_cur_;

      updateTransformedPointCloud(odom_new);

      double last_error_cost = error_cost;
      computeSquaredError(error_cost);

      double deltaF = last_error_cost - error_cost;
      double deltaL = (h.transpose() * (u * h - matJtR))[0] / 2;

      double rho = deltaF / deltaL;
      if (rho > 0) {
        // update x
        odom_cur_ = odom_new;
        // update A g
        //         updateTransformedPointCloud(odom_cur_);
        computeJtjGradError(matJtJ, matJtR, error_cost, pp_pl_total_count);
        // update u v
        u *= std::max(1.0 / 3.0, 1 - pow(2 * rho - 1, 3));
        v = 2;
      } else {
        u *= v;
        v *= 2;
        updateTransformedPointCloud(odom_cur_);
      }
    }  // One time Guass-Newton iteration
  }    // If astCornerCloudSize > 10 && lastSurfaceCloudSize > 100

  corner_points_less_sharp_.swap(last_corner_cloud_);
  surface_points_less_flat_.swap(last_surface_cloud_);

  lastCornerCloudSize = last_corner_cloud_->points.size();
  lastSurfaceCloudSize = last_surface_cloud_->points.size();

  if (lastCornerCloudSize > 10 && lastSurfaceCloudSize > 100) {
    last_corner_KDTree_.setInputCloud(last_corner_cloud_);
    last_surface_KDTree_.setInputCloud(last_surface_cloud_);
  }

  odom_acc_ = odom_cur_ * odom_acc_;

  publishResult();

  std::cout << "Frame " << frame_count_ - 1 << " to " << frame_count_
            << " match done." << std::endl;
}

void LaserOdometry::updateIndices(SE3Quat) {
  std::vector<int> pointSearchInd(1);
  std::vector<float> pointSearchSqDis(1);
  std::vector<int> indices;
  pcl::PointXYZI pointSel, tripod1, tripod2, tripod3;

  size_t cornerPointsSharpNum = corner_points_sharp_->points.size();
  size_t surfPointsFlatNum = surface_points_flat_->points.size();

  // Update point_search_corner_idxN_
  for (int i = 0; i < cornerPointsSharpNum; i++) {
    pointSel = corner_points_sharp_transformed_[i];

    last_corner_KDTree_.nearestKSearch(pointSel, 1, pointSearchInd,
                                       pointSearchSqDis);

    int closestPointInd = -1, minPointInd2 = -1;
    if (pointSearchSqDis[0] < 25) {
      closestPointInd = pointSearchInd[0];
      int closestPointScan =
          int(last_corner_cloud_->points[closestPointInd].intensity);

      float pointSqDis, minPointSqDis2 = 25;
      for (int j = closestPointInd + 1; j < cornerPointsSharpNum; j++) {
        if (int(last_corner_cloud_->points[j].intensity) >
            closestPointScan + 2.5) {
          break;
        }

        pointSqDis = calcSquaredDiff(last_corner_cloud_->points[j], pointSel);

        if (int(last_corner_cloud_->points[j].intensity) > closestPointScan) {
          if (pointSqDis < minPointSqDis2) {
            minPointSqDis2 = pointSqDis;
            minPointInd2 = j;
          }
        }
      }
      for (int j = closestPointInd - 1; j >= 0; j--) {
        if (int(last_corner_cloud_->points[j].intensity) <
            closestPointScan - 2.5) {
          break;
        }

        pointSqDis = calcSquaredDiff(last_corner_cloud_->points[j], pointSel);

        if (int(last_corner_cloud_->points[j].intensity) < closestPointScan) {
          if (pointSqDis < minPointSqDis2) {
            minPointSqDis2 = pointSqDis;
            minPointInd2 = j;
          }
        }
      }
    }

    point_search_corner_idx1_[i] = closestPointInd;
    point_search_corner_idx2_[i] = minPointInd2;
  }
  // Update point_seach_surface_idxN_
  for (int i = 0; i < surfPointsFlatNum; i++) {
    pointSel = surface_points_flat_transformed_[i];

    last_surface_KDTree_.nearestKSearch(pointSel, 1, pointSearchInd,
                                        pointSearchSqDis);
    int closestPointInd = -1, minPointInd2 = -1, minPointInd3 = -1;
    if (pointSearchSqDis[0] < 25) {
      closestPointInd = pointSearchInd[0];
      int closestPointScan =
          int(last_surface_cloud_->points[closestPointInd].intensity);

      float pointSqDis, minPointSqDis2 = 25, minPointSqDis3 = 25;
      for (int j = closestPointInd + 1; j < surfPointsFlatNum; j++) {
        if (int(last_surface_cloud_->points[j].intensity) >
            closestPointScan + 2.5) {
          break;
        }

        pointSqDis = calcSquaredDiff(last_surface_cloud_->points[j], pointSel);

        if (int(last_surface_cloud_->points[j].intensity) <= closestPointScan) {
          if (pointSqDis < minPointSqDis2) {
            minPointSqDis2 = pointSqDis;
            minPointInd2 = j;
          }
        } else {
          if (pointSqDis < minPointSqDis3) {
            minPointSqDis3 = pointSqDis;
            minPointInd3 = j;
          }
        }
      }
      for (int j = closestPointInd - 1; j >= 0; j--) {
        if (int(last_surface_cloud_->points[j].intensity) <
            closestPointScan - 2.5) {
          break;
        }

        pointSqDis = calcSquaredDiff(last_surface_cloud_->points[j], pointSel);

        if (int(last_surface_cloud_->points[j].intensity) >= closestPointScan) {
          if (pointSqDis < minPointSqDis2) {
            minPointSqDis2 = pointSqDis;
            minPointInd2 = j;
          }
        } else {
          if (pointSqDis < minPointSqDis3) {
            minPointSqDis3 = pointSqDis;
            minPointInd3 = j;
          }
        }
      }
    }

    point_seach_surface_idx1_[i] = closestPointInd;
    point_seach_surface_idx2_[i] = minPointInd2;
    point_seach_surface_idx3_[i] = minPointInd3;
  }
}

void LaserOdometry::updateTransformedPointCloud(SE3Quat odom_cur_) {
  pcl::PointXYZI pointSel, tripod1, tripod2, tripod3;

  size_t cornerPointsSharpNum = corner_points_sharp_->points.size();
  size_t surfPointsFlatNum = surface_points_flat_->points.size();

  // Update corner_points_sharp_transformed_
  for (int i = 0; i < cornerPointsSharpNum; i++) {
    pointSel = corner_points_sharp_->points[i];
    Eigen::Vector3d np =
        odom_cur_ * Eigen::Vector3d(pointSel.x, pointSel.y, pointSel.z);
    corner_points_sharp_transformed_[i].x = np[0];
    corner_points_sharp_transformed_[i].y = np[1];
    corner_points_sharp_transformed_[i].z = np[2];
  }

  // Update surface_points_flat_transformed_
  for (int i = 0; i < surfPointsFlatNum; i++) {
    pointSel = surface_points_flat_->points[i];
    Eigen::Vector3d np =
        odom_cur_ * Eigen::Vector3d(pointSel.x, pointSel.y, pointSel.z);
    surface_points_flat_transformed_[i].x = np[0];
    surface_points_flat_transformed_[i].y = np[1];
    surface_points_flat_transformed_[i].z = np[2];
  }
}

double LaserOdometry::computeSquaredError(double& error_cost) {
  pcl::PointXYZI pointSel, tripod1, tripod2, tripod3;

  error_cost = 0;

  size_t cornerPointsSharpNum = corner_points_sharp_->points.size();
  size_t surfPointsFlatNum = surface_points_flat_->points.size();

  // Matrix used in GN method
  Eigen::Matrix<double, 3, 1> matR;

  for (int i = 0; i < cornerPointsSharpNum; i++) {
    if (point_search_corner_idx2_[i] < 0) continue;

    pointSel = corner_points_sharp_transformed_[i];
    tripod1 = last_corner_cloud_->points[point_search_corner_idx1_[i]];
    tripod2 = last_corner_cloud_->points[point_search_corner_idx2_[i]];

    float x0 = pointSel.x;
    float y0 = pointSel.y;
    float z0 = pointSel.z;
    float x1 = tripod1.x;
    float y1 = tripod1.y;
    float z1 = tripod1.z;
    float x2 = tripod2.x;
    float y2 = tripod2.y;
    float z2 = tripod2.z;

    float x10 = x0 - x1;
    float y10 = y0 - y1;
    float z10 = z0 - z1;

    float x12 = x2 - x1;
    float y12 = y2 - y1;
    float z12 = z2 - z1;

    // clang-format off
        matR <<
          y12*z10 - y10*z12,
          x10*z12 - x12*z10,
          x12*y10 - x10*y12;
    // clang-format on
    double length = std::sqrt(x12 * x12 + y12 * y12 + z12 * z12);
    if (length > 1e-2) {
      double factor = 1.0 / length;
      matR *= factor;
      error_cost += matR.squaredNorm();
    }
  }  // Construct JtJ and JtR for "point-line" match

  for (int i = 0; i < surfPointsFlatNum; i++) {
    if (point_seach_surface_idx2_[i] < 0 || point_seach_surface_idx3_[i] < 0)
      continue;

    pointSel = surface_points_flat_transformed_[i];
    tripod1 = last_surface_cloud_->points[point_seach_surface_idx1_[i]];
    tripod2 = last_surface_cloud_->points[point_seach_surface_idx2_[i]];
    tripod3 = last_surface_cloud_->points[point_seach_surface_idx3_[i]];

    float x0 = pointSel.x;
    float y0 = pointSel.y;
    float z0 = pointSel.z;
    float x1 = tripod1.x;
    float y1 = tripod1.y;
    float z1 = tripod1.z;
    float x2 = tripod2.x;
    float y2 = tripod2.y;
    float z2 = tripod2.z;
    float x3 = tripod3.x;
    float y3 = tripod3.y;
    float z3 = tripod3.z;

    float x10 = x0 - x1;
    float y10 = y0 - y1;
    float z10 = z0 - z1;

    float x12 = x2 - x1;
    float y12 = y2 - y1;
    float z12 = z2 - z1;

    float x13 = x3 - x1;
    float y13 = y3 - y1;
    float z13 = z3 - z1;

    // clang-format off
        matR <<
          -x13*(y10*z12 - y12*z10),
           y13*(x10*z12 - x12*z10),
          -z13*(x10*y12 - x12*y10);
    // clang-format on
    // factor = 1 / (2 * S_{abc})
    double _2S = std::fabs((x1 * y2 - x2 * y1) + (x2 * y3 - x3 * y2) +
                           (x3 * y1 - x1 * y3));
    if (_2S > 1e-2) {
      double factor = 1.0 / _2S;
      matR *= factor;
      error_cost += matR.squaredNorm();
    }
  }  //  Construct JtJ and JtR for "point-plane" match

  return error_cost / 2;
}

void LaserOdometry::computeJtjGradError(Eigen::Matrix<double, 6, 6>& matJtJ,
                                        Eigen::Matrix<double, 6, 1>& matJtR,
                                        double& error_cost,
                                        int& pp_pl_total_count) {
  pcl::PointXYZI pointSel, tripod1, tripod2, tripod3;

  size_t cornerPointsSharpNum = corner_points_sharp_->points.size();
  size_t surfPointsFlatNum = surface_points_flat_->points.size();

  Eigen::Matrix<double, 3, 6> matJ;
  Eigen::Matrix<double, 3, 1> matR;

  matJtJ.setZero();
  matJtR.setZero();
  error_cost = 0;
  pp_pl_total_count = 0;

  for (int i = 0; i < cornerPointsSharpNum; i++) {
    if (point_search_corner_idx2_[i] < 0) continue;

    pointSel = corner_points_sharp_transformed_[i];
    tripod1 = last_corner_cloud_->points[point_search_corner_idx1_[i]];
    tripod2 = last_corner_cloud_->points[point_search_corner_idx2_[i]];

    float x0 = pointSel.x;
    float y0 = pointSel.y;
    float z0 = pointSel.z;
    float x1 = tripod1.x;
    float y1 = tripod1.y;
    float z1 = tripod1.z;
    float x2 = tripod2.x;
    float y2 = tripod2.y;
    float z2 = tripod2.z;

    float x10 = x0 - x1;
    float y10 = y0 - y1;
    float z10 = z0 - z1;

    float x12 = x2 - x1;
    float y12 = y2 - y1;
    float z12 = z2 - z1;

    // clang-format off
    matJ <<
      y0*y12 + z0*z12,         -x0*y12,         -x0*z12,    0, -z12,  y12,
              -x12*y0, x0*x12 + z0*z12,         -y0*z12,  z12,    0, -x12,
              -x12*z0,         -y12*z0, x0*x12 + y0*y12, -y12,  x12,    0;
    matR <<
      y12*z10 - y10*z12,
      x10*z12 - x12*z10,
      x12*y10 - x10*y12;
    // clang-format on
    double length = std::sqrt(x12 * x12 + y12 * y12 + z12 * z12);
    if (length > 1e-2) {
      double factor = 1.0 / length;
      matJ *= factor;
      matR *= factor;
      matJtJ += matJ.transpose() * matJ;
      matJtR += matJ.transpose() * matR;
      error_cost += matR.squaredNorm();
      ++pp_pl_total_count;
    }
  }  // Construct JtJ and JtR for "point-line" match

  for (int i = 0; i < surfPointsFlatNum; i++) {
    if (point_seach_surface_idx2_[i] < 0 || point_seach_surface_idx3_[i] < 0)
      continue;

    pointSel = surface_points_flat_transformed_[i];
    tripod1 = last_surface_cloud_->points[point_seach_surface_idx1_[i]];
    tripod2 = last_surface_cloud_->points[point_seach_surface_idx2_[i]];
    tripod3 = last_surface_cloud_->points[point_seach_surface_idx3_[i]];

    float x0 = pointSel.x;
    float y0 = pointSel.y;
    float z0 = pointSel.z;
    float x1 = tripod1.x;
    float y1 = tripod1.y;
    float z1 = tripod1.z;
    float x2 = tripod2.x;
    float y2 = tripod2.y;
    float z2 = tripod2.z;
    float x3 = tripod3.x;
    float y3 = tripod3.y;
    float z3 = tripod3.z;

    float x10 = x0 - x1;
    float y10 = y0 - y1;
    float z10 = z0 - z1;

    float x12 = x2 - x1;
    float y12 = y2 - y1;
    float z12 = z2 - z1;

    float x13 = x3 - x1;
    float y13 = y3 - y1;
    float z13 = z3 - z1;

    // clang-format off
    matJ <<
      x13*(y0*y12 + z0*z12),           -x0*x13*y12,           -x0*x13*z12,        0, -x13*z12,  x13*y12,
                -x12*y0*y13, y13*(x0*x12 + z0*z12),           -y0*y13*z12,  y13*z12,        0, -x12*y13,
                -x12*z0*z13,           -y12*z0*z13, z13*(x0*x12 + y0*y12), -y12*z13,  x12*z13,        0;
    matR <<
      -x13*(y10*z12 - y12*z10),
        y13*(x10*z12 - x12*z10),
      -z13*(x10*y12 - x12*y10);
    // clang-format on
    // factor = 1 / (2 * S_{abc})
    double _2S = std::fabs((x1 * y2 - x2 * y1) + (x2 * y3 - x3 * y2) +
                           (x3 * y1 - x1 * y3));
    if (_2S > 1e-2) {
      double factor = 1.0 / _2S;
      matJ *= factor;
      matR *= factor;
      matJtJ += matJ.transpose() * matJ;
      matJtR += matJ.transpose() * matR;
      error_cost += matR.squaredNorm();
      ++pp_pl_total_count;
    }
  }  //  Construct JtJ and JtR for "point-plane" match

  error_cost /= 2;
}

void LaserOdometry::publishResult() {
  Eigen::Quaterniond rotation = odom_acc_.rotation();
  Eigen::Vector3d translation = odom_acc_.translation();

  tf::StampedTransform tf_msg;
  tf_msg.stamp_ = time_surface_points_less_flat_;
  tf_msg.setOrigin(tf::Vector3(translation[0], translation[1], translation[2]));
  tf_msg.setRotation(
      tf::Quaternion(rotation.x(), rotation.y(), rotation.z(), rotation.w()));
  tf_msg.frame_id_ = "/camera_init";
  tf_msg.child_frame_id_ = "/laser_odom";
  tf_broadcaster_.sendTransform(tf_msg);

  nav_msgs::Odometry odom_msg;
  odom_msg.header.stamp = time_surface_points_less_flat_;
  odom_msg.header.frame_id = "/camera_init";
  odom_msg.child_frame_id = "/laser_odom";
  odom_msg.pose.pose.position.x = translation[0];
  odom_msg.pose.pose.position.y = translation[1];
  odom_msg.pose.pose.position.z = translation[2];
  odom_msg.pose.pose.orientation.x = rotation.x();
  odom_msg.pose.pose.orientation.y = rotation.y();
  odom_msg.pose.pose.orientation.z = rotation.z();
  odom_msg.pose.pose.orientation.w = rotation.w();
  laser_odom_publisher_.publish(odom_msg);

  // publish cloud results according to the input output ratio
  if (io_ratio_ < 2 || frame_count_ % io_ratio_ == 1) {
    ros::Time sweepTime = time_surface_points_less_flat_;
    //     transformToEnd(laser_cloud_);  // transform full resolution cloud
    //     to
    //     sweep end before sending it

    publishCloudMsg(laser_cloud_corner_last_publisher_, *last_corner_cloud_,
                    sweepTime, "/camera");
    publishCloudMsg(laser_cloud_surface_last_publisher_, *last_surface_cloud_,
                    sweepTime, "/camera");
    publishCloudMsg(laser_cloud_fullres_publisher_, *laser_cloud_, sweepTime,
                    "/camera");
  }
}

}  // end namespace loam
