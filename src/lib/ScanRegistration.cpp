// Copyright 2013, Ji Zhang, Carnegie Mellon University
// Further contributions copyright (c) 2016, Southwest Research Institute
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the copyright holder nor the names of its
//    contributors may be used to endorse or promote products derived from this
//    software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//
// This is an implementation of the algorithm described in the following paper:
//   J. Zhang and S. Singh. LOAM: Lidar Odometry and Mapping in Real-time.
//     Robotics: Science and Systems Conference (RSS). Berkeley, CA, July 2014.

#include "loam_velodyne/ScanRegistration.h"
#include "loam_velodyne/math_utils.h"

#include <pcl/filters/voxel_grid.h>
#include <tf/transform_datatypes.h>

namespace loam {

RegistrationParams::RegistrationParams(
    const float& scanPeriod_, const int& imuHistorySize_,
    const int& nFeatureRegions_, const int& curvatureRegion_,
    const int& maxCornerSharp_, const int& maxSurfaceFlat_,
    const float& lessFlatFilterSize_, const float& surfaceCurvatureThreshold_)
    : scanPeriod(scanPeriod_),
      imuHistorySize(imuHistorySize_),
      nFeatureRegions(nFeatureRegions_),
      curvatureRegion(curvatureRegion_),
      maxCornerSharp(maxCornerSharp_),
      maxCornerLessSharp(10 * maxCornerSharp_),
      maxSurfaceFlat(maxSurfaceFlat_),
      lessFlatFilterSize(lessFlatFilterSize_),
      surfaceCurvatureThreshold(surfaceCurvatureThreshold_){

      };

bool RegistrationParams::parseParams(const ros::NodeHandle& nh) {
  bool success = true;
  int iParam = 0;
  float fParam = 0;

  if (nh.getParam("scanPeriod", fParam)) {
    if (fParam <= 0) {
      ROS_ERROR("Invalid scanPeriod parameter: %f (expected > 0)", fParam);
      success = false;
    } else {
      scanPeriod = fParam;
      ROS_INFO("Set scanPeriod: %g", fParam);
    }
  }

  if (nh.getParam("imuHistorySize", iParam)) {
    if (iParam < 1) {
      ROS_ERROR("Invalid imuHistorySize parameter: %d (expected >= 1)", iParam);
      success = false;
    } else {
      imuHistorySize = iParam;
      ROS_INFO("Set imuHistorySize: %d", iParam);
    }
  }

  if (nh.getParam("featureRegions", iParam)) {
    if (iParam < 1) {
      ROS_ERROR("Invalid featureRegions parameter: %d (expected >= 1)", iParam);
      success = false;
    } else {
      nFeatureRegions = iParam;
      ROS_INFO("Set nFeatureRegions: %d", iParam);
    }
  }

  if (nh.getParam("curvatureRegion", iParam)) {
    if (iParam < 1) {
      ROS_ERROR("Invalid curvatureRegion parameter: %d (expected >= 1)",
                iParam);
      success = false;
    } else {
      curvatureRegion = iParam;
      ROS_INFO("Set curvatureRegion: +/- %d", iParam);
    }
  }

  if (nh.getParam("maxCornerSharp", iParam)) {
    if (iParam < 1) {
      ROS_ERROR("Invalid maxCornerSharp parameter: %d (expected >= 1)", iParam);
      success = false;
    } else {
      maxCornerSharp = iParam;
      maxCornerLessSharp = 10 * iParam;
      ROS_INFO("Set maxCornerSharp / less sharp: %d / %d", iParam,
               maxCornerLessSharp);
    }
  }

  if (nh.getParam("maxCornerLessSharp", iParam)) {
    if (iParam < maxCornerSharp) {
      ROS_ERROR("Invalid maxCornerLessSharp parameter: %d (expected >= %d)",
                iParam, maxCornerSharp);
      success = false;
    } else {
      maxCornerLessSharp = iParam;
      ROS_INFO("Set maxCornerLessSharp: %d", iParam);
    }
  }

  if (nh.getParam("maxSurfaceFlat", iParam)) {
    if (iParam < 1) {
      ROS_ERROR("Invalid maxSurfaceFlat parameter: %d (expected >= 1)", iParam);
      success = false;
    } else {
      maxSurfaceFlat = iParam;
      ROS_INFO("Set maxSurfaceFlat: %d", iParam);
    }
  }

  if (nh.getParam("surfaceCurvatureThreshold", fParam)) {
    if (fParam < 0.001) {
      ROS_ERROR(
          "Invalid surfaceCurvatureThreshold parameter: %f (expected >= 0.001)",
          fParam);
      success = false;
    } else {
      surfaceCurvatureThreshold = fParam;
      ROS_INFO("Set surfaceCurvatureThreshold: %g", fParam);
    }
  }

  if (nh.getParam("lessFlatFilterSize", fParam)) {
    if (fParam < 0.001) {
      ROS_ERROR("Invalid lessFlatFilterSize parameter: %f (expected >= 0.001)",
                fParam);
      success = false;
    } else {
      lessFlatFilterSize = fParam;
      ROS_INFO("Set lessFlatFilterSize: %g", fParam);
    }
  }

  return success;
}

ScanRegistration::ScanRegistration(const RegistrationParams& config)
    : config_(config),
      sweep_starttime_(),
      scantime_(),
      imu_start_(),
      imu_cur_(),
      imu_idx_(0),
      imu_history_(config_.imuHistorySize),
      laser_pointcloud_(),
      cornerpoint_sharp_(),
      cornerpoint_less_sharp_(),
      surfacepoint_flat_(),
      surfacepoint_less_flat_(),
      imu_trans_(4, 1),
      region_curvatures_(),
      region_labels_(),
      region_sort_indices_(),
      scan_neighbor_picked_() {}

bool ScanRegistration::setup(ros::NodeHandle& node,
                             ros::NodeHandle& privateNode) {
  if (!config_.parseParams(privateNode)) {
    return false;
  }
  imu_history_.ensureCapacity(config_.imuHistorySize);

  // subscribe to IMU topic
  imu_subscriber_ = node.subscribe<sensor_msgs::Imu>(
      "/imu/data", 50, &ScanRegistration::handleIMUMessage, this);

  // advertise scan registration topics
  laser_pointcloud_publisher_ =
      node.advertise<sensor_msgs::PointCloud2>("/velodyne_cloud_2", 2);
  cornerpoint_sharp_publisher_ =
      node.advertise<sensor_msgs::PointCloud2>("/laser_cloud_sharp", 2);
  cornerpoint_less_sharp_publisher_ =
      node.advertise<sensor_msgs::PointCloud2>("/laser_cloud_less_sharp", 2);
  surfacepoint_flat_publisher_ =
      node.advertise<sensor_msgs::PointCloud2>("/laser_cloud_flat", 2);
  surfacepoint_less_flat_publisher_ =
      node.advertise<sensor_msgs::PointCloud2>("/laser_cloud_less_flat", 2);
  imu_trans_publisher_ =
      node.advertise<sensor_msgs::PointCloud2>("/imu_trans", 5);

  return true;
}

void ScanRegistration::handleIMUMessage(
    const sensor_msgs::Imu::ConstPtr& imuIn) {
  double roll, pitch, yaw;
  tf::Quaternion orientation;
  tf::quaternionMsgToTF(imuIn->orientation, orientation);
  tf::Matrix3x3(orientation).getRPY(roll, pitch, yaw);

  Vector3 acc;
  acc.x() = float(imuIn->linear_acceleration.y - sin(roll) * cos(pitch) * 9.81);
  acc.y() = float(imuIn->linear_acceleration.z - cos(roll) * cos(pitch) * 9.81);
  acc.z() = float(imuIn->linear_acceleration.x + sin(pitch) * 9.81);

  IMUState newState;
  newState.stamp = imuIn->header.stamp;
  newState.roll = roll;
  newState.pitch = pitch;
  newState.yaw = yaw;
  newState.acceleration = acc;

  if (imu_history_.size() > 0) {
    // accumulate IMU position and velocity over time
    rotateZXY(acc, newState.roll, newState.pitch, newState.yaw);

    const IMUState& prevState = imu_history_.last();
    float timeDiff = float((newState.stamp - prevState.stamp).toSec());
    newState.position = prevState.position + (prevState.velocity * timeDiff) +
                        (0.5 * acc * timeDiff * timeDiff);
    newState.velocity = prevState.velocity + acc * timeDiff;
  }

  imu_history_.push(newState);
}

void ScanRegistration::reset(const ros::Time& scanTime, const bool& newSweep) {
  scantime_ = scanTime;

  // re-initialize IMU start index and state
  imu_idx_ = 0;
  if (hasIMUData()) {
    interpolateIMUStateFor(0, imu_start_);
  }

  // clear internal cloud buffers at the beginning of a sweep
  if (newSweep) {
    sweep_starttime_ = scanTime;

    // clear cloud buffers
    laser_pointcloud_.clear();
    cornerpoint_sharp_.clear();
    cornerpoint_less_sharp_.clear();
    surfacepoint_flat_.clear();
    surfacepoint_less_flat_.clear();

    // clear scan indices vector
    scan_indices_.clear();
  }
}

void ScanRegistration::setIMUTransformFor(const float& relTime) {
  interpolateIMUStateFor(relTime, imu_cur_);

  float relSweepTime = (scantime_ - sweep_starttime_).toSec() + relTime;
  imu_pos_shift_ = imu_cur_.position - imu_start_.position -
                   imu_start_.velocity * relSweepTime;
}

void ScanRegistration::transformToStartIMU(pcl::PointXYZI& point) {
  // rotate point to global IMU system
  rotateZXY(point, imu_cur_.roll, imu_cur_.pitch, imu_cur_.yaw);

  // add global IMU position shift
  point.x += imu_pos_shift_.x();
  point.y += imu_pos_shift_.y();
  point.z += imu_pos_shift_.z();

  // rotate point back to local IMU system relative to the start IMU state
  rotateYXZ(point, -imu_start_.yaw, -imu_start_.pitch, -imu_start_.roll);
}

void ScanRegistration::interpolateIMUStateFor(const float& relTime,
                                              IMUState& outputState) {
  double timeDiff =
      (scantime_ - imu_history_[imu_idx_].stamp).toSec() + relTime;
  while (imu_idx_ < imu_history_.size() - 1 && timeDiff > 0) {
    imu_idx_++;
    timeDiff = (scantime_ - imu_history_[imu_idx_].stamp).toSec() + relTime;
  }

  if (imu_idx_ == 0 || timeDiff > 0) {
    outputState = imu_history_[imu_idx_];
  } else {
    float ratio =
        -timeDiff /
        (imu_history_[imu_idx_].stamp - imu_history_[imu_idx_ - 1].stamp)
            .toSec();
    IMUState::interpolate(imu_history_[imu_idx_], imu_history_[imu_idx_ - 1],
                          ratio, outputState);
  }
}

void ScanRegistration::extractFeatures(const uint16_t& beginIdx) {
  // extract features from individual scans
  // 提取每条线的特征，包含平面特征和角点特征
  size_t nScans = scan_indices_.size();
  for (size_t i = beginIdx; i < nScans; i++) {
    pcl::PointCloud<pcl::PointXYZI>::Ptr surfPointsLessFlatScan(
        new pcl::PointCloud<pcl::PointXYZI>);
    size_t scanStartIdx = scan_indices_[i].first;
    size_t scanEndIdx = scan_indices_[i].second;

    // skip empty scans
    if (scanEndIdx <= scanStartIdx + 2 * config_.curvatureRegion) {
      continue;
    }

    // Quick&Dirty fix for relative point time calculation without IMU data
    /*float scanSize = scanEndIdx - scanStartIdx + 1;
    for (int j = scanStartIdx; j <= scanEndIdx; j++) {
      laser_pointcloud_[j].intensity = i + _scanPeriod * (j - scanStartIdx) /
    scanSize;
    }*/

    // reset scan buffers
    setScanBuffersFor(scanStartIdx, scanEndIdx);

    // extract features from equally sized scan regions
    for (int j = 0; j < config_.nFeatureRegions; j++) {
      size_t sp = ((scanStartIdx + config_.curvatureRegion) *
                       (config_.nFeatureRegions - j) +
                   (scanEndIdx - config_.curvatureRegion) * j) /
                  config_.nFeatureRegions;
      size_t ep = ((scanStartIdx + config_.curvatureRegion) *
                       (config_.nFeatureRegions - 1 - j) +
                   (scanEndIdx - config_.curvatureRegion) * (j + 1)) /
                      config_.nFeatureRegions -
                  1;

      // skip empty regions
      if (ep <= sp) {
        continue;
      }

      size_t regionSize = ep - sp + 1;

      // reset region buffers
      setRegionBuffersFor(sp, ep);

      // extract corner features
      int largestPickedNum = 0;
      for (size_t k = regionSize;
           k > 0 && largestPickedNum < config_.maxCornerLessSharp;) {
        size_t idx = region_sort_indices_[--k];
        size_t scanIdx = idx - scanStartIdx;
        size_t regionIdx = idx - sp;

        if (scan_neighbor_picked_[scanIdx] == 0 &&
            region_curvatures_[regionIdx] > config_.surfaceCurvatureThreshold) {
          largestPickedNum++;
          if (largestPickedNum <= config_.maxCornerSharp) {
            region_labels_[regionIdx] = CORNER_SHARP;
            cornerpoint_sharp_.push_back(laser_pointcloud_[idx]);
          } else {
            region_labels_[regionIdx] = CORNER_LESS_SHARP;
          }
          cornerpoint_less_sharp_.push_back(laser_pointcloud_[idx]);

          markAsPicked(idx, scanIdx);
        }
      }

      // extract flat surface features
      int smallestPickedNum = 0;
      for (int k = 0;
           k < regionSize && smallestPickedNum < config_.maxSurfaceFlat; k++) {
        size_t idx = region_sort_indices_[k];
        size_t scanIdx = idx - scanStartIdx;
        size_t regionIdx = idx - sp;

        if (scan_neighbor_picked_[scanIdx] == 0 &&
            region_curvatures_[regionIdx] < config_.surfaceCurvatureThreshold) {
          smallestPickedNum++;
          region_labels_[regionIdx] = SURFACE_FLAT;
          surfacepoint_flat_.push_back(laser_pointcloud_[idx]);

          markAsPicked(idx, scanIdx);
        }
      }

      // extract less flat surface features
      for (int k = 0; k < regionSize; k++) {
        if (region_labels_[k] <= SURFACE_LESS_FLAT) {
          surfPointsLessFlatScan->push_back(laser_pointcloud_[sp + k]);
        }
      }
    }

    // down size less flat surface point cloud of current scan
    pcl::PointCloud<pcl::PointXYZI> surfPointsLessFlatScanDS;
    pcl::VoxelGrid<pcl::PointXYZI> downSizeFilter;
    downSizeFilter.setInputCloud(surfPointsLessFlatScan);
    downSizeFilter.setLeafSize(config_.lessFlatFilterSize,
                               config_.lessFlatFilterSize,
                               config_.lessFlatFilterSize);
    downSizeFilter.filter(surfPointsLessFlatScanDS);

    surfacepoint_less_flat_ += surfPointsLessFlatScanDS;
  }
}

void ScanRegistration::setRegionBuffersFor(const size_t& startIdx,
                                           const size_t& endIdx) {
  // resize buffers
  size_t regionSize = endIdx - startIdx + 1;
  region_curvatures_.resize(regionSize);
  region_sort_indices_.resize(regionSize);
  region_labels_.assign(regionSize, SURFACE_LESS_FLAT);

  // calculate point curvatures and reset sort indices
  float pointWeight = -2 * config_.curvatureRegion;

  for (size_t i = startIdx, regionIdx = 0; i <= endIdx; i++, regionIdx++) {
    float diffX = pointWeight * laser_pointcloud_[i].x;
    float diffY = pointWeight * laser_pointcloud_[i].y;
    float diffZ = pointWeight * laser_pointcloud_[i].z;

    for (int j = 1; j <= config_.curvatureRegion; j++) {
      diffX += laser_pointcloud_[i + j].x + laser_pointcloud_[i - j].x;
      diffY += laser_pointcloud_[i + j].y + laser_pointcloud_[i - j].y;
      diffZ += laser_pointcloud_[i + j].z + laser_pointcloud_[i - j].z;
    }

    region_curvatures_[regionIdx] =
        diffX * diffX + diffY * diffY + diffZ * diffZ;
    region_sort_indices_[regionIdx] = i;
  }

  // sort point curvatures
  for (size_t i = 1; i < regionSize; i++) {
    for (size_t j = i; j >= 1; j--) {
      if (region_curvatures_[region_sort_indices_[j] - startIdx] <
          region_curvatures_[region_sort_indices_[j - 1] - startIdx]) {
        std::swap(region_sort_indices_[j], region_sort_indices_[j - 1]);
      }
    }
  }
}

void ScanRegistration::setScanBuffersFor(const size_t& startIdx,
                                         const size_t& endIdx) {
  // resize buffers
  size_t scanSize = endIdx - startIdx + 1;
  scan_neighbor_picked_.assign(scanSize, 0);

  // mark unreliable points as picked
  for (size_t i = startIdx + config_.curvatureRegion;
       i < endIdx - config_.curvatureRegion; i++) {
    const pcl::PointXYZI& previousPoint = (laser_pointcloud_[i - 1]);
    const pcl::PointXYZI& point = (laser_pointcloud_[i]);
    const pcl::PointXYZI& nextPoint = (laser_pointcloud_[i + 1]);

    float diffNext = calcSquaredDiff(nextPoint, point);

    if (diffNext > 0.1) {
      float depth1 = calcPointDistance(point);
      float depth2 = calcPointDistance(nextPoint);

      if (depth1 > depth2) {
        float weighted_distance =
            std::sqrt(calcSquaredDiff(nextPoint, point, depth2 / depth1)) /
            depth2;

        if (weighted_distance < 0.1) {
          std::fill_n(
              &scan_neighbor_picked_[i - startIdx - config_.curvatureRegion],
              config_.curvatureRegion + 1, 1);

          continue;
        }
      } else {
        float weighted_distance =
            std::sqrt(calcSquaredDiff(point, nextPoint, depth1 / depth2)) /
            depth1;

        if (weighted_distance < 0.1) {
          std::fill_n(&scan_neighbor_picked_[i - startIdx + 1],
                      config_.curvatureRegion + 1, 1);
        }
      }
    }

    float diffPrevious = calcSquaredDiff(point, previousPoint);
    float dis = calcSquaredPointDistance(point);

    if (diffNext > 0.0002 * dis && diffPrevious > 0.0002 * dis) {
      scan_neighbor_picked_[i - startIdx] = 1;
    }
  }
}

void ScanRegistration::markAsPicked(const size_t& cloudIdx,
                                    const size_t& scanIdx) {
  scan_neighbor_picked_[scanIdx] = 1;

  for (int i = 1; i <= config_.curvatureRegion; i++) {
    if (calcSquaredDiff(laser_pointcloud_[cloudIdx + i],
                        laser_pointcloud_[cloudIdx + i - 1]) > 0.05) {
      break;
    }

    scan_neighbor_picked_[scanIdx + i] = 1;
  }

  for (int i = 1; i <= config_.curvatureRegion; i++) {
    if (calcSquaredDiff(laser_pointcloud_[cloudIdx - i],
                        laser_pointcloud_[cloudIdx - i + 1]) > 0.05) {
      break;
    }

    scan_neighbor_picked_[scanIdx - i] = 1;
  }
}

void ScanRegistration::publishResult() {
  // publish full resolution and feature point clouds
  publishCloudMsg(laser_pointcloud_publisher_, laser_pointcloud_,
                  sweep_starttime_, "/camera");
  publishCloudMsg(cornerpoint_sharp_publisher_, cornerpoint_sharp_,
                  sweep_starttime_, "/camera");
  publishCloudMsg(cornerpoint_less_sharp_publisher_, cornerpoint_less_sharp_,
                  sweep_starttime_, "/camera");
  publishCloudMsg(surfacepoint_flat_publisher_, surfacepoint_flat_,
                  sweep_starttime_, "/camera");
  publishCloudMsg(surfacepoint_less_flat_publisher_, surfacepoint_less_flat_,
                  sweep_starttime_, "/camera");

  // publish corresponding IMU transformation information
  imu_trans_[0].x = imu_start_.pitch.rad();
  imu_trans_[0].y = imu_start_.yaw.rad();
  imu_trans_[0].z = imu_start_.roll.rad();

  imu_trans_[1].x = imu_cur_.pitch.rad();
  imu_trans_[1].y = imu_cur_.yaw.rad();
  imu_trans_[1].z = imu_cur_.roll.rad();

  Vector3 imuShiftFromStart = imu_pos_shift_;
  rotateYXZ(imuShiftFromStart, -imu_start_.yaw, -imu_start_.pitch,
            -imu_start_.roll);

  imu_trans_[2].x = imuShiftFromStart.x();
  imu_trans_[2].y = imuShiftFromStart.y();
  imu_trans_[2].z = imuShiftFromStart.z();

  Vector3 imuVelocityFromStart = imu_cur_.velocity - imu_start_.velocity;
  rotateYXZ(imuVelocityFromStart, -imu_start_.yaw, -imu_start_.pitch,
            -imu_start_.roll);

  imu_trans_[3].x = imuVelocityFromStart.x();
  imu_trans_[3].y = imuVelocityFromStart.y();
  imu_trans_[3].z = imuVelocityFromStart.z();

  publishCloudMsg(imu_trans_publisher_, imu_trans_, sweep_starttime_,
                  "/camera");
}

}  // end namespace loam
