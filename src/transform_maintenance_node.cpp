#include <ros/ros.h>
#include "loam_velodyne/TransformMaintenance.h"

#ifdef __linux__
#include <sys/resource.h>
#include <time.h>
#endif

/** Main node entry point. */
int main(int argc, char **argv) {
  ros::init(argc, argv, "transformMaintenance");
  ros::NodeHandle node;
  ros::NodeHandle privateNode("~");

  loam::TransformMaintenance transMaintenance;

  if (transMaintenance.setup(node, privateNode)) {
    // initialization successful
    ros::spin();
  }

#ifdef __linux__
  timespec cpu_timespec = {};
  clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &cpu_timespec);
  rusage usage;
  getrusage(RUSAGE_SELF, &usage);
  std::cout << "TransformMaintenance elapsed CPU time: "
            << (cpu_timespec.tv_sec + 1e-9 * cpu_timespec.tv_nsec)
            << " s, peak memory usage: " << usage.ru_maxrss << " KiB\n";
#endif

  return 0;
}
